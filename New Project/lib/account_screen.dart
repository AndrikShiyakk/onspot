import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import './change_name.dart';
import './change_email.dart';
import './change_password_screen.dart';

class AccountScreen extends StatelessWidget {
  static const routeName = '/account_screen';

  

  @override
  Widget build(BuildContext context) {
    void goToChangePassword() {
    Navigator.of(context).pushReplacementNamed(ChangePasswordScreen.routeName);
  }

    void goBack() {
      Navigator.of(context).pushReplacementNamed('/');
    }

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(207, 39, 44, 1),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: goBack,
          ),
          title: Text(
            'Profile',
            style: TextStyle(
              fontFamily: 'Encode Sans',
              fontSize: 18.sp,
              fontWeight: FontWeight.w500,
            ),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24.w),
                child: Column(children: [
                  SizedBox(
                    height: 15.h,
                  ),
                  CircleAvatar(
                    radius: 40.h,
                    backgroundColor: Color.fromRGBO(222, 219, 230, 1),
                    child: CircleAvatar(
                      radius: 37.h,
                      backgroundColor: Colors.white,
                      child: ClipOval(
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Image.asset('assets/images/no_avatar.png'),
                        ),
                      ),
                    ),
                  ),

                 
                  SizedBox(
                    height: 19.h,
                  ),
                  Container(
                    child: InkWell(
                      onTap: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/images/camera.png',
                            width: 18.w,
                          ),
                          SizedBox(
                            width: 3.w,
                          ),
                          Text(
                            'Add photo',
                            style: TextStyle(
                              fontFamily: 'Encode Sans',
                              fontWeight: FontWeight.w400,
                              fontSize: 18.sp,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 38.h,
                  ),
                  ChangeName(),
                  // :
                  // TextField(
                  //   decoration: InputDecoration(
                  //     // hintText: name,
                  //     border: OutlineInputBorder(),
                  //   ),
                  //   // controller: nameController,
                  // ),
                  SizedBox(
                    height: 22.h,
                  ),
                  ChangeEmail(),
                  SizedBox(
                    height: 42.h,
                  ),
                ]),
              ),
              Divider(),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24.w),
                child: Column(children: [
                  SizedBox(
                    height: 18.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Change Password',
                        style: TextStyle(
                          fontFamily: 'Encode Sans',
                          fontWeight: FontWeight.w400,
                          fontSize: 18.sp,
                        ),
                      ),
                      InkWell(
                        onTap: goToChangePassword,
                        child: Image.asset(
                          'assets/images/arrow_forward.png',
                          height: 18.w,
                          width: 10.w,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 24.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Settings',
                        style: TextStyle(
                          fontFamily: 'Encode Sans',
                          fontWeight: FontWeight.w400,
                          fontSize: 18.sp,
                        ),
                      ),
                      InkWell(
                        onTap: () {},
                        child: Image.asset(
                          'assets/images/arrow_forward.png',
                          height: 18.w,
                          width: 10.w,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 22.h,
                  ),
                ]),
              ),
              Divider(),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24.w),
                child: Column(
                  children: [
                    SizedBox(
                      height: 18.h,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/images/logout.png',
                            height: 20.w,
                            width: 20.w,
                          ),
                          Text(
                            'Logout',
                            style: TextStyle(
                              fontFamily: 'Encode Sans',
                              fontWeight: FontWeight.w400,
                              fontSize: 18.sp,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
