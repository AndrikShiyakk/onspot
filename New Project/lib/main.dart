
import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import './account_screen.dart';
import './inspections.dart';
import './change_password_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Overview(),
      routes: {
        AccountScreen.routeName: (ctx) => AccountScreen(),
        ChangePasswordScreen.routeName: (ctx) => ChangePasswordScreen(),
      },
    );
  }
}

class Overview extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    void goToAccount() {
      Navigator.of(context).pushNamed(AccountScreen.routeName);
    }

    ScreenUtil.init(context,
        designSize: Size(375, 667), allowFontScaling: true);
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(
      //     'Profile',
      //     style: TextStyle(
      //       fontFamily: 'Encode Sans',
      //       fontWeight: FontWeight.w500,
      //       fontSize: 18.h,
      //     ),
      //   ),
      //   centerTitle: true,
      //   leading: Icon(Icons.arrow_back_ios),
      //   backgroundColor: Color.fromRGBO(207, 39, 44, 1),
      // ),
      body: Inspections(goToAccount),
    );
  }
}
