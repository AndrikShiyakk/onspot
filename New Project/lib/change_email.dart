import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';


class ChangeEmail extends StatefulWidget {
  @override
  _ChangeEmailState createState() => _ChangeEmailState();
}

class _ChangeEmailState extends State<ChangeEmail> {
  bool _chEmail = false;
  var _emailController = TextEditingController();
  var _email = 'dgipich@gmail.com';

  _changingEmail() {
    
    

    setState(() {
      _chEmail = !_chEmail;

      if (_emailController.text != null && _emailController.text != '') {
        _email = _emailController.text;
      }
    });
  }

  

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            child: Text(
              'Email',
              textAlign: TextAlign.start,
              style: TextStyle(
                fontFamily: 'Encode Sans',
                fontWeight: FontWeight.w400,
                fontSize: 12.sp,
                color: Color.fromRGBO(126, 131, 142, 1),
              ),
            ),
          ),
          SizedBox(
            height: 8.h,
          ),
          _chEmail == false
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      _email,
                      style: TextStyle(
                        fontFamily: 'Encode Sans',
                        fontWeight: FontWeight.w400,
                        fontSize: 16.sp,
                      ),
                    ),
                    InkWell(
                      onTap: _changingEmail,
                      child: Image.asset(
                        'assets/images/pencil.png',
                        width: 16.w,
                        height: 16.w,
                      ),
                    ),
                  ],
                )
              : TextField(
                  onEditingComplete: _changingEmail,
                  decoration: InputDecoration(
                    hintText: _email,
                    border: OutlineInputBorder(),
                  ),
                  controller: _emailController,
                ),
        ],
      ),
    );
  }
}
